<?php

namespace App\Http\Controllers;

use App\Http\Requests\BiddingStoreRequest;
use App\Repositories\BiddingRepository;
use Exception;
use Illuminate\Http\Request;

/**
 * Class BiddingController
 * @package App\Http\Controllers
 */
class BiddingController extends Controller
{
    /**
     * @var BiddingRepository
     */
    private $biddingRepository;

    /**
     * BiddingController constructor.
     * @param BiddingRepository $biddingRepository
     */
    public function __construct(BiddingRepository $biddingRepository)
    {

        $this->biddingRepository = $biddingRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bidding.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bidding.create');
    }

    /**
     * @param BiddingStoreRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(BiddingStoreRequest $request)
    {
        try {
            $this->biddingRepository->create($request->all());
        }catch(Exception $e){
            $msg = ['message' => 'Not was Created!'];
            logger('Not was Created, Error: '.$e->getMessage());
            return view('bidding.index',compact('msg'));
        }

        $msg = ['message' => 'Created Successfully!'];
        return view('bidding.index',compact('msg'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
