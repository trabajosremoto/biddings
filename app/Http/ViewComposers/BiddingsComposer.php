<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 16/02/19
 * Time: 11:25 PM
 */

namespace App\Http\ViewComposers;


use App\Repositories\BiddingRepository;

class BiddingsComposer
{
    /**
     * @var BiddingRepository
     */
    private $biddingRepository;

    /**
     * BiddingsComposer constructor.
     * @param BiddingRepository $biddingRepository
     */
    public function __construct(BiddingRepository $biddingRepository)
    {
        $this->biddingRepository = $biddingRepository;
    }

    public function compose($view)
    {
        $biddings = $this->biddingRepository->search()->get()->sortByDesc('created_at');
        $view->with(compact('biddings'));
    }
}