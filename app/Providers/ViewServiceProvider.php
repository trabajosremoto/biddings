<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 16/02/19
 * Time: 11:09 PM
 */

namespace App\Providers;

use App\Http\ViewComposers\BiddingsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('bidding.index', BiddingsComposer::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}