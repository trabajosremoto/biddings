<?php

namespace App\Models;

use App\Presenters\ProviderPresenter;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * Class Provider
 * @package App\Models
 */
class Provider extends Model implements HasPresenter
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'url',
        'api_url',
        'address',
        'facebook',
        'twitter',
        'linkedin',
        'instagram',
        'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function biddings()
    {
        return $this->belongsTo(Bidding::class);
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return ProviderPresenter::class;
    }
}
