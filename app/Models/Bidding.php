<?php

namespace App\Models;

use App\Presenters\BiddingPresenter;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * Class Bidding
 * @package App\Models
 */
class Bidding extends Model implements HasPresenter
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'type',
        'location',
        'show_salary',
        'min_salary',
        'max_salary',
        'status',
        'highlight',
        'visited',
        'user_id',
        'provider_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return BiddingPresenter::class;
    }
}
