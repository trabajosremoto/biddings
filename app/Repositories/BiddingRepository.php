<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 16/02/19
 * Time: 10:48 PM
 */

namespace App\Repositories;


use App\Models\Bidding;

class BiddingRepository extends AbstractRepository
{
    /**
     * BiddingRepository constructor.
     * @param Bidding $model
     */
    public function __construct(Bidding $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $params
     * @param bool $distinct
     * @param bool $count
     * @param string $orderby
     * @param string $ascDesc
     * @return mixed
     */
    public function search(array $params = [], $distinct = true, $count = false)
    {
        $query = $this->model->select('biddings.*');

        if ($distinct){
            $query = $query->distinct();
        }

        $joins = collect();

        //
        // params filters here
        //

        $joins->each(function ($item, $key) use (&$query) {
            $item = json_decode($item);
            $query->join($key, $item->first, '=', $item->second, $item->join_type);
        });

        if ($count) {
            return $query->count('biddings.id');
        }

        return $query;

    }

}