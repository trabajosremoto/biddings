<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 16/02/19
 * Time: 10:43 PM
 */

namespace App\Repositories;


use App\Models\Provider;

class ProviderRepository extends AbstractRepository
{
    /**
     * ProviderRepository constructor.
     * @param Provider $model
     */
    public function __construct(Provider $model)
    {
        $this->model = $model;
    }

    public function search(array $params = [], $distinct = true, $count = false)
    {
        $query = $this->model->select('providers.*');

        if ($distinct){
            $query = $query->distinct();
        }

        $joins = collect();

        //
        // params filters here
        //

        $joins->each(function ($item, $key) use (&$query) {
            $item = json_decode($item);
            $query->join($key, $item->first, '=', $item->second, $item->join_type);
        });

        if ($count) {
            return $query->count('providers.id');
        }

        return $query->orderBy('providers.name', 'asc');
    }
}