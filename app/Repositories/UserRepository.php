<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 16/02/19
 * Time: 10:35 PM
 */

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends AbstractRepository
{
    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param Collection $joins
     * @param $table
     * @param $first
     * @param $second
     * @param string $join_type
     */
    private function addJoin(Collection &$joins, $table, $first, $second, $join_type = 'inner')
    {
        if (!$joins->has($table)) {
            $joins->put($table, json_encode(compact('first', 'second', 'join_type')));
        }
    }

    /**
     * Search method, here are the params to create custom requests to DB.
     * @param array $params
     * @return mixed
     */
    public function search(array $params = [], $distinct = true, $count = false)
    {
        $query = $this->model->select('users.*');

        if ($distinct){
            $query = $query->distinct();
        }

        $joins = collect();

        //
        // params filters here
        //

        $joins->each(function ($item, $key) use (&$query) {
            $item = json_decode($item);
            $query->join($key, $item->first, '=', $item->second, $item->join_type);
        });

        if ($count) {
            return $query->count('users.id');
        }

        return $query->orderBy('users.name', 'asc');

    }

}