<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 16/02/19
 * Time: 10:04 AM
 */

namespace App\Presenters;


use Carbon\Carbon;
use McCool\LaravelAutoPresenter\BasePresenter;

class UserPresenter extends BasePresenter
{
    public function created_at()
    {
        return Carbon::parse($this->wrappedObject->created_ad)->format('d-M-Y');
    }

}