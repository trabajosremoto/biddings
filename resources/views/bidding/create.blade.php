@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
      <div class="col">
         @if ($errors->any())
            <div class="alert alert-danger">
               <ul>
                  @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
         @endif
         <div class="card p-3">
            <div class="row">
               <div class="col-md-8">
                  {{ link_to('bidding' , $title = "Back", $attributes = ['class' => 'btn btn-success'], $secure = null) }}
               </div>
               <div align="right" class="col-md-4">
                  <h5>{{ 'Create New Biddings' }}</h5>
               </div>
            </div>
         </div>
            {!! Form::open(['route' => 'bidding.store'  ]) !!}
               <div class="row p-2">
                  <div class="col-md-3">Title</div>
                  <div class="col-md-9">
                     {{ Form::text('title','',['class' => 'form-control']) }}
                  </div>
               </div>
               <div class="row p-2">
                  <div class="col-md-3">Description</div>
                  <div class="col-md-9">
                     {{ Form::textarea('description','',['class' => 'form-control','rows'=>'4','cols'=>'20']) }}
                  </div>
               </div>
               <div class="row p-2">
                  <div class="col-md-3">Type</div>
                  <div class="col-md-9">
                     {{ Form::text('type','',['class' => 'form-control']) }}
                  </div>
               </div>
               <div class="row p-2">
                  <div class="col-md-3">Location</div>
                  <div class="col-md-9">
                     {{ Form::textarea('location','',['class' => 'form-control','rows'=>'4','cols'=>'20']) }}
                  </div>
               </div>
               <div class="row p-2">
                  <div class="col-md-3">Show Salary</div>
                  <div class="col-md-9">
                     {{ Form::select('show_salary',[true => 'Yes',false => 'No'],'0',['class' => 'form-control','id' => 'show_salary']) }}
                  </div>
               </div>
               <div id="minSalary" style="display: none" class="row p-2">
                  <div class="col-md-3">Minimal Salary</div>
                  <div class="col-md-9">{{ Form::number('min_salary','',['class' => 'form-control','id' => 'min_salary']) }}</div>
               </div>
               <div id="maxSalary" style="display: none" class="row p-2">
                  <div class="col-md-3">Maximal Salary</div>
                  <div class="col-md-9">{{ Form::number('max_salary','',['class' => 'form-control','id' => 'max_salary']) }}</div>
               </div>
               <div class="row p-2">
                  <div class="col-md-3">Status</div>
                  <div class="col-md-9">
                     {{ Form::select('status',[true => 'Active',false => 'No Active'],'1',['class' => 'form-control']) }}
                  </div>
               </div>
               <div class="row p-2">
                  <div class="col-md-3">Highlight</div>
                  <div class="col-md-9">
                     {{ Form::select('highlight',['1' => 'Yes','0' => 'No'],'0',['class' => 'form-control']) }}
                  </div>
               </div>
               <div  class="card">
                  <div align="center" class="card-footer">
                     {{ Form::submit('Create',['class' => 'btn btn-primary']) }}
                  </div>
               </div>
            {!! Form::close() !!}

      </div>
   </div>
</div>
<script src="{{ mix('js/biddings.js') }}" defer></script>
@endsection




