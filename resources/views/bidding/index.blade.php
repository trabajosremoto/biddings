@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @if(isset($msg))
            <div class="alert alert-sucess">
                <ul>
                    @foreach ($msg as $message)
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif
        <a href="{{ route('bidding.create')  }}">Create New Bidding</a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
        </tr>
        </thead>
        <tbody>
        @foreach($biddings as $bidding)
        <tr>
            <td>{{ $bidding->id }}</td>
            <td>{{ $bidding->title }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection