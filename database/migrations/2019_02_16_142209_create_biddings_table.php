<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiddingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biddings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description');
            $table->string('type')->nullable();
            $table->string('location')->nullable();
            $table->boolean('show_salary')->nullable();
            $table->string('min_salary')->nullable();
            $table->string('max_salary')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('highlight')->default(false);
            $table->integer('visited')->default(0);

            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('provider_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');

            // Creating index
            $table->index('user_id', 'users_bidding_foreign');
            $table->index('provider_id', 'providers_bidding_foreign');

            // Timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biddings');
    }
}
