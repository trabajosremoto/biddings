<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Provider::class, function (Faker $faker) {
    return [
        'name'       => $faker->company,
        'email'      => $faker->companyEmail,
        'url'        => $faker->url,
        'api_url'    => $faker->url,
        'address'    => $faker->address,
        'facebook'   => 'https://facebook.com/'.$faker->userName,
        'twitter'    => 'https://twitter.com/'.$faker->userName,
        'linkedin'   => 'https://linkedin.com/'.$faker->userName,
        'instagram'  => 'https://instagram.com/'.$faker->userName,
        'status'     => true
    ];
});
