<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Bidding::class, function (Faker $faker) {

    $creator = rand(1, 10);

    if ($creator < 5 ) {
        $user_id = rand(1, 50);
        $provider_id = null;
    } else {
        $user_id = null;
        $provider_id = rand(1, 50);
    }

    return [
        'title'        => $faker->text(60),
        'description'  => $faker->paragraph(60, true),
        'type'         => "On Site",
        'location'     => $faker->address(),
        'show_salary'  => true,
        'min_salary'   => rand(0, 2500),
        'max_salary'   => rand(2500, 10000),
        'status'       => true,
        'visited'      => rand(0, 500),
        'highlight'    => $faker->boolean,
        'user_id'      => $user_id,
        'provider_id'  => $provider_id
    ];
});
