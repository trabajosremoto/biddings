<?php

use App\Models\Bidding;
use Illuminate\Database\Seeder;

/**
 * Class BiddingsTableSeeder
 */
class BiddingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Bidding::class, 500)->create();
    }
}
