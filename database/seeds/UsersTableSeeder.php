<?php

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $henry = [
            'name'  => 'Henry Esteban Leon Gomez',
            'email' => 'helg18@gmail.com'
        ];

        $jean = [
            'name'  => 'Jean Carlos Nunez Hernandez',
            'email' => 'jeancarlosn2008@gmail.com'
        ];

        factory(User::class)->create($henry);
        factory(User::class)->create($jean);
        factory(User::class, 48)->create();
    }
}
