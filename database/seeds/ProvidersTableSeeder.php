<?php

use App\Models\Provider;
use Illuminate\Database\Seeder;

/**
 * Class ProvidersTableSeeder
 */
class ProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Provider::class, 50)->create();
    }
}
